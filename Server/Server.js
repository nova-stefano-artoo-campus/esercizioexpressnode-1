var express = require("express");
var app = express();
var path = require("path");
var bodyParser = require("body-parser");
const PORT = 3000;

app.use(bodyParser.urlencoded( {
  extended: false
}));
app.use(bodyParser.json());

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "..", "Client", "Index.html"));
});

var utenti = require("../Utenti/Utenti.js");
app.use("/users", utenti);

app.listen(PORT, function() {
  console.log("server start at http://localhost:" + PORT);
});
