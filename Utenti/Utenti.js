var express = require("express");
var router = express.Router();
var listaUtenti = require("./Database.json");
var jsonfile = require("jsonfile");
var path = require("path");

router.get("/", function(req, res) {
  res.status(200).json(listaUtenti);
});

router.get("/id/:id", function(req, res) {
  var id = req.params.id;
  var utente = listaUtenti.find(function(el) {
    return el.id == id;
  });
  if(utente) {
    res.status(200).json(utente);
  } else {
    res.status(404).send("utente non trovato");
  }
});

router.get("/sesso", function(req, res) {
  var sesso = req.query.sesso;
  var listaFiltrata = listaUtenti.filter(function(el) {
    return el.sesso == sesso;
  });
  if(listaFiltrata.length) {
    res.status(200).json(listaFiltrata);
  } else {
    res.status(404).send("nessun utente di sesso: " + sesso);
  }
});

router.get("/nome", function(req, res) {
  var nome = req.query.nome;
  var listaFiltrata = listaUtenti.filter(function(el) {
    return el.nome == nome;
  });
  if(listaFiltrata.length) {
    res.status(200).json(listaFiltrata);
  } else {
    res.status(404).send("nessun utente di nome: " + nome);
  }
});

/*Non possibile utilizzare dal browser la procedura di elimina, ma utilizzare Postman
ponendo nella barra di "send" "DELETE" e non "GET"
oltre al solito percorso "http://localhost:3000(in questo caso "/users/id/(esempio 10)")"*/
router.delete("/id/:id", function(req, res) {
  var id = req.params.id;
  var utente = listaUtenti.find(function(el) {
    return el.id == id;
  });
  var indice = listaUtenti.indexOf(utente);
  listaUtenti.splice(indice,1);
  res.json(listaUtenti);
  jsonfile.writeFile(path.join(__dirname, "Database.json"), listaUtenti, function(err){});
});

/*Non possibile utilizzare dal browser la procedura di inserimento, ma utilizzare Postman
ponendo nella barra di "send" "POST" e non "GET"
oltre al solito percorso "http://localhost:3000(in questo caso "/users/")"*/
router.post("/", function(req, res) {
  var nuovo = req.body;
  var max = 0;
  for(let i=0; i<listaUtenti.length; i++) {
    if(listaUtenti[i].id >= max) {
      max = listaUtenti[i].id;
    }
  }
  nuovo.id = max + 1;
  listaUtenti.push(nuovo);
  jsonfile.writeFile(path.join(__dirname, "Database.json"), listaUtenti, function(err){});
  res.json(listaUtenti);
});

/*Non possibile utilizzare dal browser la procedura di aggiornamento, ma utilizzare Postman
ponendo nella barra di "send" "PUT" e non "GET"
oltre al solito percorso "http://localhost:3000(in questo caso "/users/id/(esempio 10)")"*/
router.put("/id/:id", function(req, res) {
  var id = req.params.id;
  var aggiornato = req.body;
  var vecchio = listaUtenti.find(function(el) {
    return el.id == id;
  });
  var indice = listaUtenti.indexOf(vecchio);
  listaUtenti.splice(indice,1,aggiornato);
  res.json(listaUtenti);
  jsonfile.writeFile(path.join(__dirname, "Database.json"), listaUtenti, function(err){});
});

module.exports = router;
